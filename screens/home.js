import React from 'react';
import { Text, View, StyleSheet, Image, ScrollView, FlatList, AlertIOS, Dimensions, TouchableOpacity, AsyncStorage } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
const { width, height } = Dimensions.get('window');
import { fetchTransactions } from 'finwice/redux/actions/fetchTransactions'
import { fetchGoals } from 'finwice/redux/actions/fetchGoals'
import { fetchGoalStatus } from 'finwice/redux/actions/fetchGoalStatus'
import numeral from 'numeral'

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isEmpty: false,
    };
  }
  static navigationOptions = ({ navigation }) => {
     return {
       header: null,
       headerTitle: 'SAFE',
       headerTitleStyle: {
         fontFamily: 'Source Sans Pro',
         fontSize: 19,
         fontWeight: 'normal',
         color: '#FFF',
         alignSelf: 'center',
       },
       headerStyle: {
         backgroundColor: '#20C287',
         borderBottomWidth: 0,
         borderColor: 'rgba(102, 51, 255, 100)'
       }
     }
  }

  componentDidMount(){
    this.props.fetchTransactions()
    this.props.fetchGoals()
    this.props.fetchGoalStatus()
  }

  processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1]
    }));
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={(this.props.goal_status.payload.title === 'Danger') ? styles.headerDanger : styles.header}>

          <View style={{ marginTop: 34, marginBottom: 7, alignItems: 'center'}}>
            <Text style={[styles.headerText, {marginTop: 20, fontSize: 16}]}>{this.props.goal_status.payload.title}</Text>
            <Text style={styles.amount}>{numeral(this.props.goal_status.payload.tatal).format('0,0.00')}</Text>
          </View>

          <View style={{ marginTop: 7, marginBottom: 7}}>
            <Text style={[styles.headerText, {marginLeft: 18, marginBottom: 7}]}>Your Goals</Text>
          </View>

          <View style={[styles.headerSectionTwo, {}]}>
            <ScrollView contentContainerStyle={styles.topScroller} horizontal>
            {<FlatList
              horizontal
              data={this.props.goals.payload}
              extraData={this.state}
              renderItem={({item, index}) => (
                <TouchableOpacity onPress={() => this.props.navigation.push('UpdateGoal', {
                  id: item.id,
                  cat_id: item.category_id,
                  amount: item.target_amount,
                  title: item.title,
                  start_date: item.starting_date,
                  end_date: item.due_date
                })}>
                  <View style={styles.miniCards}>
                    <View style={styles.miniCardsSectionOne}>
                      <Text style={styles.cardDesc}>{item.title}</Text>
                      <Text style={styles.cardAmount}>{item.target_amount}</Text>
                    </View>
                    {/*<View style={styles.miniCardsSectionTwo}>
                      <Image source={require('./icons/tick-icon.png')} style={styles.tickIcon} />
                    </View>*/}
                  </View>
                </TouchableOpacity>
              )}
              keyExtractor={(item, index) => index.toString()}
            />}

            <TouchableOpacity onPress={() => this.props.navigation.push('StoreGoal')}>
              <View style={styles.miniCards}>
                <View style={styles.miniCardsSectionOne}>
                  <Text style={[styles.cardDesc, {fontSize: 14, left: 8, top: 4}]}>
                    Add New Goal
                  </Text>
                </View>
                <View style={styles.miniCardsSectionTwo}>
                  <Image source={require('./icons/add-icon-slim.png')} style={styles.tickIcon} />
                </View>
              </View>
            </TouchableOpacity>

            </ScrollView>
          </View>
        </View>
        {/*(this.state.isEmpty) ?
          <View style={[styles.adSection, {height: 200}]}>
            <Text style={styles.adHeading}>GET STARTED</Text>
            <Text style={[styles.adMainText, {fontSize: 16}]}>{'Connect with Bank of Maldives Internet Banking and start importing your data.'}</Text>
            <Text style={[styles.adMainText, {fontWeight: 'normal', fontSize: 14, color: '#CBCBCB'}]}>{'We will help you understand your spending habbits and help you achieve your financial goals with the help of FineWice algorithms using machine learning.'}</Text>
            <TouchableOpacity style={{backgroundColor: 'white', marginTop: 6, padding: 10, borderRadius: 20, width: 148, justifyContent: 'center', alignItems: 'center'}} onPress={() => this.props.navigation.navigate('BMLConnect')}>
              <Text style={[styles.adLink, {fontSize: 14, fontWeight: '500', color: '#444'}]}>Connect with Bank</Text>
            </TouchableOpacity>
          </View>
          :
          <View style={[styles.adSection]}>
            <Text style={styles.adHeading}>INVEST YOUR MONEY!</Text>
            <Text style={styles.adMainText}>{'Invest 5000 on X Company IPO get a return of 5% annually.'}</Text>
            <TouchableOpacity onPress={() => AlertIOS.alert('Opens Ad Link')}>
              <Text style={styles.adLink}>Learn more</Text>
            </TouchableOpacity>
          </View>
      */  }


        {(!this.props.transactions.payload.length > 0) ?
          <Text style={[styles.feedheading, {textAlign: 'center', top: 200, fontSize: 16}]}>{`I don't have any transaction. Please feed me.`}</Text>
          :
          <Text style={styles.feedheading}>Transactions</Text>
        }
        <ScrollView contentContainerStyle={styles.mainFeed}>
          <FlatList
            data={this.props.transactions.payload}
            extraData={this.state}
            renderItem={({item, index}) => (
              <TouchableOpacity onPress={() => this.props.navigation.push('UpdateTransaction', {
                  id: item.id,
                  cat_id: item.category_id,
                  location: item.location,
                  amount: item.amount,
                  date: item.transaction_date,
                  type: item.type
                })}>
                  <View style={styles.feedCards}>
                    <View style={styles.feedCardsSectionOne}>
                      <Image source={{uri: item.categories.icon}} style={styles.homeFeedIcon} />
                    </View>
                    <View style={styles.feedCardsSectionTwo}>
                      <Text style={styles.feedCardsHeading}>{item.location}</Text>
                      <Text style={styles.feedCardsDate}>{item.transaction_date}</Text>
                    </View>
                    <View style={styles.feedCardsSectionThree}>
                      <Text style={(item.type === 'Expense') ? styles.feedCardsAmountExpense : styles.feedCardsAmount}>{numeral(item.amount).format('0,0.00')}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
          />

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F8F5'
  },
  header: {
    width: width,
    backgroundColor: '#20C287'
  },
  headerDanger: {
    width: width,
    backgroundColor: '#CD4646'
  },
  headerSectionOne: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  headerSectionTwo: {
    flex: 0,
    width: width,
    justifyContent: 'center',
    bottom: 4
  },
  amount: {
    fontSize: 30,
    color: 'white',
    fontWeight: '500',
    fontFamily: 'Source Sans Pro'
  },
  topScroller: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 20,
    paddingBottom: 12
  },
  headerText: {
    fontSize: 15,
    fontWeight: '500',
    color: 'white',
    fontFamily: 'Source Sans Pro'
  },
  miniCards: {
    height: 70,
    width: 170,
    marginLeft: 20,
    backgroundColor: 'white',
    borderRadius: 6,
    flexDirection: 'row'
  },
  miniCardsSectionOne: {
    padding: 12,
    flex: 2,
    justifyContent: 'center',
  },
  miniCardsSectionTwo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tickIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain'
  },
  cardDesc: {
    fontSize: 11,
    fontWeight: '500',
    marginBottom: 8,
    fontFamily: 'Source Sans Pro'
  },
  cardAmount: {
    fontSize: 18,
    fontWeight: '500',
    color: '#20C287',
    fontFamily: 'Source Sans Pro'
  },
  adSection: {
    justifyContent: 'center',
    width: width,
    height: 100,
    backgroundColor: '#5B5B5B',
    padding: 20
  },
  adHeading: {
    fontSize: 10,
    fontWeight: 'normal',
    color: 'white',
    fontFamily: 'Source Sans Pro',
    marginBottom: 6,
    letterSpacing: 0.6
  },
  adMainText: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white',
    fontFamily: 'Source Sans Pro',
    marginBottom: 6

  },
  adLink: {
    fontSize: 10,
    fontWeight: 'normal',
    color: '#20C287',
    fontFamily: 'Source Sans Pro'
  },
  mainFeed: {
    padding: 20,
    alignItems: 'center'
  },
  feedheading: {
    padding: 20,
    fontSize: 14,
    fontWeight: '500',
    color: '#444',
    fontFamily: 'Source Sans Pro'
  },
  feedCards: {
    height: 70,
    width: width-40,
    backgroundColor: 'white',
    borderRadius: 6,
    flexDirection: 'row',
    marginBottom: 16
  },
  homeFeedIcon: {
    width: 48,
    height: 48,
    resizeMode: 'contain'
  },
  feedCardsSectionOne: {
    left: 8,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  feedCardsSectionTwo: {
    flex: 3,
    padding: 8,
    marginLeft: 4,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  feedCardsSectionThree: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    right: 8
  },
  feedCardsHeading: {
    fontSize: 14,
    fontWeight: '500',
    color: '#444',
    fontFamily: 'Source Sans Pro',
    marginBottom: 4
  },
  feedCardsDate: {
    fontSize: 12,
    fontWeight: 'normal',
    color: '#CBCBCB',
    fontFamily: 'Source Sans Pro'
  },
  feedCardsAmount: {
    right: 8,
    fontSize: 14,
    fontWeight: '500',
    color: '#20C287',
    fontFamily: 'Source Sans Pro'
  },
  feedCardsAmountExpense: {
    right: 8,
    fontSize: 14,
    fontWeight: '500',
    color: '#C22020',
    fontFamily: 'Source Sans Pro'
  },
})

function mapStateToProps(state) {
    return {
        transactions: state.transactions,
        goals: state.goals,
        goal_status: state.goal_status
    }
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ fetchTransactions, fetchGoals, fetchGoalStatus }, dispatch)
    }
}

//export default HOME
export default connect(mapStateToProps, mapDispatchToProps)(Home)
