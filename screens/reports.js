import React from 'react';
import { Text, View, StyleSheet, Image, ScrollView, AlertIOS, Dimensions, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window');
import { Pie } from 'react-native-pathjs-charts'

class Reports extends React.Component {

  static navigationOptions = ({ navigation }) => {
     return {
       headerTitle: 'REPORTS',
       headerTitleStyle: {
         fontFamily: 'Source Sans Pro',
         fontSize: 17,
         fontWeight: 'normal',
         color: '#FFF',
         alignSelf: 'center',
       },
       headerStyle: {
         backgroundColor: '#20C287',
         borderBottomWidth: 0,
         borderColor: 'rgba(102, 51, 255, 100)'
       }
     }
  }

  render() {
    let data = [{
      "name": "Cafe' & Restaurants",
      "population": 4000
    }, {
      "name": "Travel",
      "population": 4500
    }, {
      "name": "Rent",
      "population": 3400
    }, {
      "name": "Others",
      "population": 1600
    }]
    let options = {
      margin: {
        top: 20,
        left: 20,
        right: 20,
        bottom: 20
      },
      width: 350,
      height: 350,
      color: '#2980B9',
      r: 50,
      R: 150,
      legendPosition: 'topLeft',
      animate: {
        type: 'oneByOne',
        duration: 200,
        fillTransition: 3
      },
      label: {
        fontFamily: 'Source Sans Pro',
        fontSize: 8,
        fontWeight: true,
        color: '#ECF0F1'
      }
    }

    return (
      <View style={styles.container}>
        <Text style={styles.title}>Your spendings</Text>
        <Pie
          data={data}
          options={options}
          accessorKey="population"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F8F5',
    alignItems: 'center',
    paddingTop: 20,
    paddingLeft: 40
  },
  title: {
    fontSize: 16,
    right: 14,
    fontWeight: '500',
    color: '#333',
    fontFamily: 'Source Sans Pro'
  },

})
export default Reports
