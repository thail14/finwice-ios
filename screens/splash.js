import React from 'react'
import { Text, View, Image, StyleSheet, AsyncStorage } from 'react-native'

class Splash extends React.Component {

  componentDidMount(){
    setTimeout(() => {
      AsyncStorage.getItem('isLoggedIn', (err, result)=>{
        if (result === '200') {
          this.props.navigation.navigate('Tabs')
        }
        else {
          this.props.navigation.navigate('Login')
        }
      })
    }, 1000)
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('./icons/logo.png')} style={styles.logo}/>
        <Text style={styles.slogan}>Keeping up with your Finances</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#20C287'
  },
  logo: {
    width: 128,
    height: 50,
    resizeMode: 'contain'
  },
  slogan: {
    fontSize: 14,
    fontWeight: '500',
    color: '#F6F9F6',
    opacity: 0.75,
    width: 140,
    textAlign: 'center',
    marginTop: 8,
  },
})

export default Splash
