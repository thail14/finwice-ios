import React from 'react';
import { Text, View, StyleSheet, Image, ScrollView, AlertIOS, Dimensions, TouchableOpacity, TextInput } from 'react-native';
const { width, height } = Dimensions.get('window');

class BMLConnect extends React.Component {
constructor(props) {
  super(props);

  this.state = {
    username: '',
    password: ''
  }
}
  static navigationOptions = ({ navigation }) => {
     return {
       headerLeft: (
         <View style={{flexDirection: 'row'}}>
          <View style={{width: width-width*0.90, alignItems: 'center'}}>
            <TouchableOpacity style={{left: 10, width: 60, height: 32}}onPress={() => navigation.goBack()}>
              <Image source={require('./icons/back.png')} style={{left: 20, width: 23, height: 23, resizeMode: 'contain'}} />
            </TouchableOpacity>
          </View>
         </View>
       ),
       headerTitle: 'Connect Bank',
       headerTitleStyle: {
         fontFamily: 'Source Sans Pro',
         fontSize: 17,
         fontWeight: 'normal',
         color: '#FFF',
         alignSelf: 'center',
       },
       headerStyle: {
         backgroundColor: '#20C287',
         borderBottomWidth: 0,
         borderColor: 'rgba(102, 51, 255, 100)'
       }
     }
  }

  processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
      statusCode: res[0],
      data: res[1]
      }));
  }

  fetchData(){
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{width: width, padding: 34}}>
          <Text style={styles.headerText}>
            {`Login using your Internet Banking Credentials`}
          </Text>
          <Text style={styles.headerSubText}>
            {`Your credentials will only be stored in this device and used to sync and process transaction data.`}
          </Text>
        </View>

        <View style={styles.loginBox}>
          <View style={styles.inputBoxInput}>
            <TextInput
             style={styles.inputBoxInput}
             placeholder={'Username'}
             onChangeText={(username) => this.setState({username})}
            />
          </View>
          <Text> </Text>
          <View style={styles.inputBoxInput}>
            <TextInput
             style={styles.inputBoxInput}
             placeholder={'Password'}
             secureTextEntry
             onChangeText={(password) => this.setState({password})}
            />
          </View>
        <TouchableOpacity onPress={() => this.fetchData()} style={[styles.button, {marginTop: 18}]}>
            <Text style={styles.buttonText}>Import my Data</Text>
          </TouchableOpacity>
        </View>
        <View style={{width: width, padding: 34}}>
          <Text style={styles.headerSubText}>
            {`Your credentials will only be stored in this device and used to sync and process transaction data.`}
          </Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F8F5',
    alignItems: 'center',
    paddingTop: 20
  },
  loginBox: {
    width: width-60,
    height: 250,
    borderRadius: 12,
    backgroundColor: 'white',
    marginTop: 24,
    marginBottom: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginBoxSectionOne: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBoxSectionTwo: {
    flex: 3,
    alignItems: 'center'
  },
  loginBoxSectionThree: {
    flex: 1,
    alignItems: 'center'
  },
  registerText: {
    top: 12,
    fontSize: 24,
    fontWeight: 'normal',
    color: '#353A41',
    fontFamily: 'Source Sans Pro'

  },
  inputBox: {
    height: 46,
    width: width-100,
    marginTop: 32,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: '#DCE6F0'
  },
  inputBoxIcon: {
    marginLeft: 4,
    top: 2,
    width: 24,
    height: 24,
    resizeMode: 'contain'
  },
  inputBoxInput: {
    width: width-100,
    height: 46,
    color: '#9FA7B3',
    fontSize: 18,
    marginLeft: 6,
    bottom: 2,
    fontFamily: 'Source Sans Pro'
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width-100,
    height: 50,
    backgroundColor: '#20C287',
    borderRadius: 8,
    bottom: 8
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '500',
    color: 'white',
    fontFamily: 'Source Sans Pro'

  },
  textButton:{
    marginTop: 18,
    fontSize: 15,
    fontWeight: '500',
    color: '#F6F9F6',
    fontFamily: 'Source Sans Pro'

  },
  headerText: {
    fontSize: 18,
    fontWeight: 'normal',
    color: '#232524',
    fontFamily: 'Source Sans Pro',
    marginBottom: 14
  },
  headerSubText: {
    fontSize: 16,
    fontWeight: 'normal',
    color: '#5B5B5B',
    fontFamily: 'Source Sans Pro',
  }
})
export default BMLConnect
