import React from 'react';
import { StackActions, NavigationActions } from 'react-navigation';
import { Text, View, StyleSheet, Image, TextInput, ScrollView, FlatList, AlertIOS, Dimensions, TouchableOpacity, AsyncStorage } from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import moment from 'moment'
import { DatePickerDialog } from 'react-native-datepicker-dialog'
import DateTimePicker from 'react-native-modal-datetime-picker';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchGoals } from 'finwice/redux/actions/fetchGoals'

const { width, height } = Dimensions.get('window');

class UpdateGoal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      amount: '',
      title: '',
      categories: [],
      selectedId: 1000,
      isDateTimePickerVisible: false,
      startDate: '',
      endDate: '',
      dobText: 'Start Date',
      dobDate: null,
      journeyText: 'Due Date',
      journeyDate: null
    };

    this.confirmAlert = this.confirmAlert.bind(this)

  }

  confirmAlert(){
    AlertIOS.alert(
      'Are you sure ?',
      'This goal will be deleted if confirmed. This action cannot be reversed',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('cancelled'),
        },
        {
          text: 'Confirm',
          onPress: () => this.deleteGoal(),
        }
      ]
    );
  }

  /**
   * DOB textbox click listener
   */
  onDOBPress = () => {

    let dobDate = this.state.dobDate;

    if(!dobDate || dobDate == null){
      dobDate = new Date();
      this.setState({
        dobDate: dobDate
      });
    }

    //To open the dialog
    this.refs.dobDialog.open({
       date: dobDate,
       minDate: new Date() //To restirct past date
    });

  }

  /**
   * Call back for dob date picked event
   *
   */
  onDOBDatePicked = (date) => {
    this.setState({
      dobDate: date,
      dobText: moment(date).format('DD MMM, YYYY')
    });
  }


  /**
   * Journey date textbox click listener
   */
  onJourneyDatePress = () => {

    let journeyDate = this.state.journeyDate;

    if(!journeyDate || journeyDate == null){
      journeyDate = new Date();
      this.setState({
        journeyDate: journeyDate
      });
    }

    //To open the dialog
    this.refs.journeyDialog.open({
      date: journeyDate,
      minDate: new Date() //To restirct past date
    });

  }

  /**
   * Call back for journey date picked event
   *
   */
  onJourneyDatePicked = (date) => {
    this.setState({
      journeyDate: date,
      journeyText: moment(date).format('DD MMM, YYYY')
    });
  }

  static navigationOptions = ({ navigation }) => {
     return {
       headerRight: (
         <View style={{paddingRight: 5}}>
           <TouchableOpacity onPress={() => navigation.state.params.handleDeleteButton()} style={{padding: 20}}>
             <Image source={require('../icons/trash-icon.png')} style={styles.headerIcons} />
           </TouchableOpacity>
         </View>
       ),
       headerLeft: (
         <View style={{marginRight: 0}}>
           <TouchableOpacity onPress={() => navigation.pop()} style={{backgroundColor: 'transparent', padding: 25, paddingRight: 30}}>
             <Image source={require('../icons/back.png')} style={styles.headerIcons} />
           </TouchableOpacity>
         </View>
       ),
       headerTitle: 'Update Goals',
       headerTitleStyle: {
         fontFamily: 'Source Sans Pro',
         fontSize: 19,
         fontWeight: 'normal',
         color: '#FFF',
         alignSelf: 'center',
       },
       headerStyle: {
         backgroundColor: '#20C287',
         borderBottomWidth: 0,
         borderColor: 'rgba(102, 51, 255, 100)'
       }
     }
  }

  componentDidMount(){
    this.setState({
      dobDate: new Date(this.props.navigation.state.params.start_date),
      journeyDate: new Date(this.props.navigation.state.params.end_date),
      dobText: moment(this.props.navigation.state.params.start_date).format('DD MMM, YYYY'),
      journeyText: moment(this.props.navigation.state.params.end_date).format('DD MMM, YYYY'),
      title: this.props.navigation.state.params.title,
      amount: this.props.navigation.state.params.amount.toString(),
      selectedId: this.props.navigation.state.params.cat_id
    })

    this.props.navigation.setParams({
     handleDeleteButton: this.confirmAlert,
    })

    //this.getCategories()
  }

  processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1]
    }));
  }

  getCategories(){
    this.setState({isLoading: false})
    var url = apiHost + '/users/categories'
    fetch(url, { method: 'GET', headers: { 'Authorization': 'Bearer ' + JWT, 'Content-Type': 'application/json' }})
    .then(this.processResponse)
    .then(res => {
      const { statusCode, data } = res
      if (statusCode === 200) {
        this.setState({ categories: data, isLoading: false })
      }

    })
  }

  saveGoal(){
    // alert(this.state.selectedId)
    // return true
    fetch(apiHost+'/users/goals/' + this.props.navigation.state.params.id, {
       method: 'PUT',
       headers: {
         'Content-Type': 'application/json',
         'Authorization': 'Bearer ' + JWT
       },
       body: JSON.stringify({
          title: this.state.title,
          starting_date: this.state.dobDate,
          due_date: this.state.journeyDate,
          target_amount: this.state.amount,
          category_id: this.state.selectedId
       }),
     })
     .then(this.processResponse)
     .then(res => {
       const { statusCode, data } = res
       if (statusCode === 200) {
         AlertIOS.alert('Goal updated successfully')
         this.props.fetchGoals()
         this.props.navigation.pop()
       }
       else {
         AlertIOS.alert('Unable to update the goal. Please try again')
       }

     }).catch(error => AlertIOS.alert('Failed', 'Please fill all the required fields'))
  }

  validate(){
    if (this.state.dobDate === null || this.state.journeyDate === null) {
      AlertIOS.alert('Failed', 'Please fill all the required fields')
    }
    else {
      this.saveGoal()
    }
  }

  onSelect(id){
    this.setState({selectedId: id})
  }

  deleteGoal(){
    fetch(apiHost+'/users/goals/' + this.props.navigation.state.params.id, {
       method: 'DELETE',
       headers: {
         'Content-Type': 'application/json',
         'Authorization': 'Bearer ' + JWT
       }
     })
     .then(this.processResponse)
     .then(res => {
       const { statusCode, data } = res

       if (statusCode === 200) {
         this.props.fetchGoals()
         AlertIOS.alert('goal deleted successfully')
         this.props.navigation.pop()
       }
       else {
         AlertIOS.alert('Unable to deleted the goal')
       }

     }).catch(error => AlertIOS.alert('Failed', 'Something went wrong!'))

  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
            <View style={styles.sectionTwo}>
              <View style={styles.loginBox}>
                <View style={styles.loginBoxSectionTwo}>
                  {/*<Text style={{alignSelf: 'center', marginTop: 12, marginBottom: 12, fontSize: 18, fontWeight: '400', fontFamily: 'Source Sans Pro' }}>Choose a category</Text>

                  <View style={styles.categoriesView}>
                    <FlatList
                      horizontal
                      data={this.state.categories}
                      extraData={this.state}
                      renderItem={({item, index}) => (
                        <TouchableOpacity onPress={() => this.onSelect(item.id)}>
                          <View style={styles.iconContainer}>
                            <Image source={{uri: item.icon}} style={(item.id === this.state.selectedId) ? styles.selectedIcon : styles.icon} />
                            <Text style={(item.id === this.state.selectedId) ? styles.selectedCategoryLabel : styles.categoryLabel}>{item.name}</Text>
                          </View>
                        </TouchableOpacity>
                      )}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  </View>*/}

                  {/* TITLE FIELD */}
                  <View style={styles.inputBox}>
                    <View style={styles.inputBoxInput}>
                      <TextInput
                       style={styles.inputBoxInput}
                       placeholder={'Goal Title'}
                       onChangeText={(title) => this.setState({title})}
                       value={this.state.title}
                      />
                    </View>
                  </View>

                  {/* AMOUNT FIELD */}
                  <View style={styles.inputBox}>
                    <View style={styles.inputBoxInput}>
                      <TextInput
                       keyboardType={'number-pad'}
                       style={styles.inputBoxInput}
                       placeholder={'Target Amount'}
                       onChangeText={(amount) => this.setState({amount})}
                       value={this.state.amount}
                      />
                    </View>
                  </View>

                  <TouchableOpacity onPress={this.onDOBPress.bind(this)} >
                    <View style={styles.inputBox}>
                      <View style={styles.inputBoxInput}>
                        <Text style={styles.inputBoxInput}>{ this.state.dobText }</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={this.onJourneyDatePress.bind(this)} >
                    <View style={styles.inputBox}>
                      <View style={styles.inputBoxInput}>
                        <Text style={styles.inputBoxInput}>{ this.state.journeyText}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <DatePickerDialog ref="dobDialog" onDatePicked={this.onDOBDatePicked.bind(this)} />
                  <DatePickerDialog ref="journeyDialog" onDatePicked={this.onJourneyDatePicked.bind(this)} />

                </View>
                <View style={{marginTop: 24}}/>
                <View style={styles.loginBoxSectionThree}>
                  <TouchableOpacity onPress={() => this.validate()} style={[styles.button, {marginBottom: 40}]}>
                    <Text style={styles.buttonText}>UPDATE</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F8F5',
  },
  sectionOne: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionTwo: {
    flex: 3,
    backgroundColor: 'transparent',
    alignItems: 'center'
  },
  logo: {
    width: 128,
    height: 50,
    resizeMode: 'contain'
  },
  slogan: {
    fontSize: 14,
    fontWeight: '500',
    color: '#F6F9F6',
    opacity: 0.75,
    width: 140,
    textAlign: 'center',
    marginTop: 8,
    fontFamily: 'Source Sans Pro'
  },
  loginBox: {
    width: width-60,
    flex: 1,
    borderRadius: 12,
    backgroundColor: 'transparent',
    marginTop: 24,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBoxSectionOne: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBoxSectionTwo: {
    flex: 3,
    alignItems: 'center'
  },
  loginBoxSectionThree: {
    flex: 1,
    alignItems: 'center'
  },
  registerText: {
    fontSize: 24,
    fontWeight: 'normal',
    color: '#353A41',
    fontFamily: 'Source Sans Pro'
  },
  inputBox: {
    height: 46,
    width: width-100,
    marginTop: 32,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: '#DCE6F0'
  },
  inputBoxIcon: {
    marginLeft: 4,
    top: 2,
    width: 24,
    height: 24,
    resizeMode: 'contain'
  },
  inputBoxInput: {
    width: width-140,
    height: 46,
    color: '#333',
    fontSize: 18,
    marginLeft: 6,
    bottom: 2,
    fontFamily: 'Source Sans Pro'
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 8,
    width: width-100,
    height: 50,
    backgroundColor: '#20C287',
    borderRadius: 8
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '500',
    color: 'white',
    fontFamily: 'Source Sans Pro'
  },
  textButton:{
    marginTop: 24,
    fontSize: 15,
    fontWeight: '500',
    color: '#F6F9F6',
    fontFamily: 'Source Sans Pro'
  },
  icon: {
    width: 48,
    height: 48,
    marginRight: 8,
    marginBottom: 8,
    resizeMode: 'contain'
  },
  selectedIcon: {
    width: 54,
    height: 54,
    marginRight: 8,
    marginBottom: 8,
    resizeMode: 'contain',
    borderRadius: 54/2
  },
  categoriesView: {
    width: width-49,
    alignItems: 'center',
    flexDirection: 'row',
    padding: 20,
    flexWrap: 'wrap',
    backgroundColor: '#eff2ef',
    borderRadius: 12,
  },
  iconContainer: {
    padding: 7,
    marginRight: 14,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  selectedIconContainer: {
    padding: 7,
    marginRight: 14,
    alignItems: 'center',
    justifyContent: 'space-between',
    opacity: 100
  },
  categoryLabel: {
    textAlign: 'center',
    fontFamily: 'Source Sans Pro'
  },
  selectedCategoryLabel: {
    textAlign: 'center',
    fontFamily: 'Source Sans Pro',
    color: '#20C287',
    fontSize: 16
  },
  headerIcons: {
  width: 22,
  height: 22,
  resizeMode: 'contain',
  tintColor: '#FFF'
},

})

function mapStateToProps(state) {
    return {
        goals: state.goals
    }
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ fetchGoals }, dispatch)
    }
}

//export default ADD NEW TRANSACTION
export default connect(mapStateToProps, mapDispatchToProps)(UpdateGoal)
