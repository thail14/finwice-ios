import React from 'react'
import { Text, AlertIOS, AsyncStorage, ScrollView, View, Image, TextInput, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window');

class Register extends React.Component {
constructor(props) {
  super(props)

  this.state = {
    name: '',
    mobile: '',
    password: '',
    isLoading: false,
  }
}
  static navigationOptions = ({navigation}) => ({
    header: null,
  })

  processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1]
    }));
  }

  doRegister(){
    this.setState({isLoading: true})
    fetch(apiHost+'/auth/register', {
       method: 'POST',
       headers: {
         Accept: 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
         name: this.state.name,
         mobile_number: this.state.mobile,
         password: this.state.password,
       }),
     })
     .then(this.processResponse)
     .then(res => {
       const { statusCode, data } = res
       if (statusCode === 200) {
         this.setState({isLoading: false})
         AlertIOS.alert('Login Success', 'You have been logged in successfully!')
         var loginAttrs = [['isLoggedIn', '200']]
         AsyncStorage.multiSet(loginAttrs, (err) => {
           setGlobalJWT(data.token)
           this.props.navigation.navigate('Tabs')
         })
       }
       else {

         AlertIOS.alert(statusCode.toString(), JSON.stringify(data.messages))
         this.setState({isLoading: false})
       }
      })

  }

  componentDidMount(){
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.sectionOne}>
            <Image source={require('../icons/logo.png')} style={styles.logo}/>
            <Text style={styles.slogan}>Keeping up with your Finances</Text>
          </View>
          <View style={styles.sectionTwo}>
            <View style={styles.loginBox}>
              <View style={styles.loginBoxSectionOne}>
                <Text style={styles.registerText}>Register</Text>
              </View>
              <View style={styles.loginBoxSectionTwo}>
                {/* NAME FIELD */}
                <View style={styles.inputBox}>
                  <View style={styles.inputBoxIcon}>
                    <Image source={require('../icons/person-icon.png')} style={styles.inputBoxIcon}/>
                  </View>
                  <View style={styles.inputBoxInput}>
                    <TextInput
                     style={styles.inputBoxInput}
                     placeholder={'Name'}
                     onChangeText={(name) => this.setState({name})}
                    />
                  </View>
                </View>
                {/* PHONE NUMBER FIELD */}
                <View style={styles.inputBox}>
                  <View style={styles.inputBoxIcon}>
                    <Image source={require('../icons/phone-icon.png')} style={styles.inputBoxIcon}/>
                  </View>
                  <View style={styles.inputBoxInput}>
                    <TextInput
                     keyboardType={'number-pad'}
                     style={styles.inputBoxInput}
                     placeholder={'Mobile Number'}
                     onChangeText={(mobile) => this.setState({mobile})}
                    />
                  </View>
                </View>
                {/* PASSWORD FIELD */}
                <View style={styles.inputBox}>
                  <View style={styles.inputBoxIcon}>
                    <Image source={require('../icons/key-icon.png')} style={styles.inputBoxIcon}/>
                  </View>
                  <View style={styles.inputBoxInput}>
                    <TextInput
                     style={styles.inputBoxInput}
                     placeholder={'Create a password'}
                     secureTextEntry
                     onChangeText={(password) => this.setState({password})}
                    />
                  </View>
                </View>
              </View>
              <View style={styles.loginBoxSectionThree}>
                <TouchableOpacity onPress={() => this.doRegister()} style={styles.button}>
                  <Text style={styles.buttonText}>Register</Text>
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
              <Text style={styles.textButton}>Login</Text>
            </TouchableOpacity>
          </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#20C287'
  },
  sectionOne: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionTwo: {
    flex: 3,
    backgroundColor: 'transparent',
    alignItems: 'center'
  },
  logo: {
    width: 128,
    height: 50,
    resizeMode: 'contain'
  },
  slogan: {
    fontSize: 14,
    fontWeight: '500',
    color: '#F6F9F6',
    opacity: 0.75,
    width: 140,
    textAlign: 'center',
    marginTop: 8,
    fontFamily: 'Source Sans Pro'
  },
  loginBox: {
    width: width-60,
    height: height/1.9,
    borderRadius: 12,
    backgroundColor: 'white',
    marginTop: 24
  },
  loginBoxSectionOne: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBoxSectionTwo: {
    flex: 3,
    alignItems: 'center'
  },
  loginBoxSectionThree: {
    flex: 1,
    alignItems: 'center'
  },
  registerText: {
    fontSize: 24,
    fontWeight: 'normal',
    color: '#353A41',
    fontFamily: 'Source Sans Pro'
  },
  inputBox: {
    height: 46,
    width: width-100,
    marginTop: 32,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: '#DCE6F0'
  },
  inputBoxIcon: {
    marginLeft: 4,
    top: 2,
    width: 24,
    height: 24,
    resizeMode: 'contain'
  },
  inputBoxInput: {
    width: width-140,
    height: 46,
    color: '#9FA7B3',
    fontSize: 18,
    marginLeft: 6,
    bottom: 2,
    fontFamily: 'Source Sans Pro'
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 8,
    width: width-100,
    height: 50,
    backgroundColor: '#20C287',
    borderRadius: 8
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '500',
    color: 'white',
    fontFamily: 'Source Sans Pro'
  },
  textButton:{
    marginTop: 24,
    fontSize: 15,
    fontWeight: '500',
    color: '#F6F9F6',
    fontFamily: 'Source Sans Pro'
  }
})
export default Register
