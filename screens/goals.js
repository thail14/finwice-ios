import React from 'react';
import { Text, View, Image, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

class Goals extends React.Component {

  static navigationOptions = ({ navigation }) => {
     return {
       headerTitle: 'Settings',
       headerTitleStyle: {
         fontFamily: 'Source Sans Pro',
         fontSize: 17,
         fontWeight: 'normal',
         color: '#FFF',
         alignSelf: 'center',
       },
       headerStyle: {
         backgroundColor: '#20C287',
         borderBottomWidth: 0,
         borderColor: 'rgba(102, 51, 255, 100)'
       }
     }
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
        <Image source={require('./icons/goals-screen.png')} style={{width: width, height: height, resizeMode: 'contain', bottom: 130}} />
      </View>
    );
  }
}

export default Goals
