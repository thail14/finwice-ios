import React from 'react';
import { StackActions, NavigationActions } from 'react-navigation';
import { Text, View, StyleSheet, Image, TextInput, ScrollView, FlatList, AlertIOS, Dimensions, TouchableOpacity, AsyncStorage } from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import moment from 'moment'
import DateTimePicker from 'react-native-modal-datetime-picker';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchTransactions } from 'finwice/redux/actions/fetchTransactions'
import { fetchGoalStatus } from 'finwice/redux/actions/fetchGoalStatus'

const { width, height } = Dimensions.get('window');

var radio_props = [
  {label: 'Expense', value: 'Expense' },
  {label: 'Income', value: 'Income' }
];

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'StoreTransaction' })],
});

class StoreTransaction extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      amount: '',
      location: '',
      categories: [],
      selectedId: 1000,
      value: 'Expense',
      isDateTimePickerVisible: false,
      date: 'Date'
    };
  }

  static navigationOptions = ({ navigation }) => {
     return {
       headerTitle: 'Add Transacations',
       headerTitleStyle: {
         fontFamily: 'Source Sans Pro',
         fontSize: 19,
         fontWeight: 'normal',
         color: '#FFF',
         alignSelf: 'center',
       },
       headerStyle: {
         backgroundColor: '#20C287',
         borderBottomWidth: 0,
         borderColor: 'rgba(102, 51, 255, 100)'
       }
     }
  }

  componentDidMount(){
    this.setState({date: new Date()})
    this.getCategories()
  }

  processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1]
    }));
  }

  getCategories(){
    this.setState({isLoading: false})
    var url = apiHost + '/users/categories'
    fetch(url, { method: 'GET', headers: { 'Authorization': 'Bearer ' + JWT, 'Content-Type': 'application/json' }})
    .then(this.processResponse)
    .then(res => {
      const { statusCode, data } = res
      if (statusCode === 200) {
        this.setState({ categories: data, isLoading: false })
      }

    })
  }

  saveTransaction(){

    fetch(apiHost+'/users/transactions', {
       method: 'POST',
       headers: {
         'Content-Type': 'application/json',
         'Authorization': 'Bearer ' + JWT
       },
       body: JSON.stringify({
          type: this.state.value,
          transaction_date: this.state.date,
          amount: this.state.amount,
          category_id: this.state.selectedId,
          location: this.state.location
       }),
     })
     .then(this.processResponse)
     .then(res => {
       const { statusCode, data } = res
       if (statusCode === 200) {
         AlertIOS.alert('Transacation added successfully')
         this.props.navigation.dispatch(resetAction)
         this.props.fetchTransactions()
         this.props.fetchGoalStatus()
         this.props.navigation.navigate('Feed')
       }
       else {
         AlertIOS.alert('Unable to add the transaction. Please try again')
       }

     }).catch(error => AlertIOS.alert('Failed', 'Please fill all the required fields'))
  }

  onSelect(id){
    this.setState({selectedId: id})
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    console.log('A date has been picked: ', typeof(date));

    this.setState({ date: date.toString() })
    this._hideDateTimePicker();
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView >
            <View style={styles.sectionTwo}>
              <View style={styles.loginBox}>
                <View style={styles.loginBoxSectionTwo}>
                  <Text style={{alignSelf: 'center', marginTop: 12, marginBottom: 12, fontSize: 18, fontWeight: '400', fontFamily: 'Source Sans Pro' }}>Choose a category</Text>

                  <View style={styles.categoriesView}>
                    <FlatList
                      horizontal
                      data={this.state.categories}
                      extraData={this.state}
                      renderItem={({item, index}) => (
                        <TouchableOpacity onPress={() => this.onSelect(item.id)}>
                          <View style={styles.iconContainer}>
                            <Image source={{uri: item.icon}} style={(item.id === this.state.selectedId) ? styles.selectedIcon : styles.icon} />
                            <Text style={(item.id === this.state.selectedId) ? styles.selectedCategoryLabel : styles.categoryLabel}>{item.name}</Text>
                          </View>
                        </TouchableOpacity>
                      )}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  </View>
                  {/* AMOUNT FIELD */}
                  <View style={styles.inputBox}>
                    <View style={styles.inputBoxInput}>
                      <TextInput
                       keyboardType={'number-pad'}
                       style={styles.inputBoxInput}
                       placeholder={'Amount'}
                       onChangeText={(amount) => this.setState({amount})}
                      />
                    </View>
                  </View>
                  {/* LOCATION FIELD */}
                  <View style={styles.inputBox}>
                    <View style={styles.inputBoxInput}>
                      <TextInput
                       style={styles.inputBoxInput}
                       placeholder={'Location'}
                       onChangeText={(location) => this.setState({location})}
                      />
                    </View>
                  </View>
                  {/* DATE FIELD */}
                  <TouchableOpacity onPress={this._showDateTimePicker}>
                    <View style={styles.inputBox}>
                      <View style={styles.inputBoxInput}>
                        <Text style={styles.inputBoxInput}>{ moment(this.state.date).format("MMM Do YY") }</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                  />
                  {/* TYPE FORM */}
                  <View style={{marginTop: 20}}/>
                  <RadioForm
                    animation={false}
                    formHorizontal
                    radio_props={radio_props}
                    initial={0}
                    onPress={(value) => {this.setState({value:value})}}
                    buttonColor={'#20C287'}
                    selectedButtonColor={'#20C287'}
                    labelStyle={{ fontFamily: 'Source Sans Pro', marginRight: 20}}
                  />

                </View>
                <View style={{marginTop: 24}}/>
                <View style={styles.loginBoxSectionThree}>
                  <TouchableOpacity onPress={() => this.saveTransaction()} style={styles.button}>
                    <Text style={styles.buttonText}>SAVE</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F8F5'
  },
  sectionOne: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionTwo: {
    flex: 3,
    backgroundColor: 'transparent',
    alignItems: 'center'
  },
  logo: {
    width: 128,
    height: 50,
    resizeMode: 'contain'
  },
  slogan: {
    fontSize: 14,
    fontWeight: '500',
    color: '#F6F9F6',
    opacity: 0.75,
    width: 140,
    textAlign: 'center',
    marginTop: 8,
    fontFamily: 'Source Sans Pro'
  },
  loginBox: {
    width: width-60,
    flex: 1,
    borderRadius: 12,
    backgroundColor: 'transparent',
    marginTop: 24,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBoxSectionOne: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBoxSectionTwo: {
    flex: 3,
    alignItems: 'center'
  },
  loginBoxSectionThree: {
    flex: 1,
    alignItems: 'center'
  },
  registerText: {
    fontSize: 24,
    fontWeight: 'normal',
    color: '#353A41',
    fontFamily: 'Source Sans Pro'
  },
  inputBox: {
    height: 46,
    width: width-100,
    marginTop: 32,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: '#DCE6F0'
  },
  inputBoxIcon: {
    marginLeft: 4,
    top: 2,
    width: 24,
    height: 24,
    resizeMode: 'contain'
  },
  inputBoxInput: {
    width: width-140,
    height: 46,
    color: '#333',
    fontSize: 18,
    marginLeft: 6,
    bottom: 2,
    fontFamily: 'Source Sans Pro'
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 8,
    width: width-100,
    height: 50,
    backgroundColor: '#20C287',
    borderRadius: 8
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '500',
    color: 'white',
    fontFamily: 'Source Sans Pro'
  },
  textButton:{
    marginTop: 24,
    fontSize: 15,
    fontWeight: '500',
    color: '#F6F9F6',
    fontFamily: 'Source Sans Pro'
  },
  icon: {
    width: 48,
    height: 48,
    marginRight: 8,
    marginBottom: 8,
    resizeMode: 'contain'
  },
  selectedIcon: {
    width: 54,
    height: 54,
    marginRight: 8,
    marginBottom: 8,
    resizeMode: 'contain',
    borderRadius: 54/2
  },
  categoriesView: {
    width: width-49,
    alignItems: 'center',
    flexDirection: 'row',
    padding: 20,
    flexWrap: 'wrap',
    backgroundColor: '#eff2ef',
    borderRadius: 12,
  },
  iconContainer: {
    padding: 7,
    marginRight: 14,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  selectedIconContainer: {
    padding: 7,
    marginRight: 14,
    alignItems: 'center',
    justifyContent: 'space-between',
    opacity: 100
  },
  categoryLabel: {
    textAlign: 'center',
    fontFamily: 'Source Sans Pro'
  },
  selectedCategoryLabel: {
    textAlign: 'center',
    fontFamily: 'Source Sans Pro',
    color: '#20C287',
    fontSize: 16
  }
})

function mapStateToProps(state) {
    return {
        transactions: state.transactions,
        goal_status: state.goal_status
    }
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ fetchTransactions, fetchGoalStatus }, dispatch)
    }
}

//export default ADD NEW TRANSACTION
export default connect(mapStateToProps, mapDispatchToProps)(StoreTransaction)
