import React from 'react';
import { Text, View, AsyncStorage, TouchableOpacity } from 'react-native';

class Settings extends React.Component {

  static navigationOptions = ({ navigation }) => {
     return {
       headerTitle: 'Settings',
       headerTitleStyle: {
         fontFamily: 'Source Sans Pro',
         fontSize: 17,
         fontWeight: 'normal',
         color: '#FFF',
         alignSelf: 'center',
       },
       headerStyle: {
         backgroundColor: '#20C287',
         borderBottomWidth: 0,
         borderColor: 'rgba(102, 51, 255, 100)'
       }
     }
  }

  doLogout(){
    AsyncStorage.clear()
    this.props.navigation.navigate('Login')
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <TouchableOpacity onPress={() => this.doLogout()}>
          <Text style={{fontSize: 34, fontWeight: '500'}}>LOGOUT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Settings
