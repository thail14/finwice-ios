import {AppRegistry, AsyncStorage} from 'react-native';
import {
  createStackNavigator,
} from 'react-navigation';
import {
  createStore,
  applyMiddleware,
  combineReducers,
} from 'redux';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';
import { Provider, connect } from 'react-redux';
import React from 'react';

import {Navigations} from './App';
import {name as appName} from './app.json';
import store from './redux/store'

global.apiHost = 'http://iloo.xyz/v1';

AsyncStorage.getItem('AUTH_TOKEN', (err, JWT)=>{
  global.JWT = JWT;
})

global.setGlobalJWT = function(JWT) {
  global.JWT = JWT
}

const App = reduxifyNavigator(AppNavigator, "root");

const mapStateToProps = (state) => ({
  state: state.nav,
});

const AppWithNavigationState = connect(mapStateToProps)(App);

class Root extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}


AppRegistry.registerComponent(appName, () => Root);
