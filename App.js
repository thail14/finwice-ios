import React from 'react';
import {StyleSheet, Image, Dimensions, Platform, AlertIOS, TouchableWithoutFeedback} from 'react-native';
import { createBottomTabNavigator,  createStackNavigator} from 'react-navigation';
import Splash from './screens/splash'
import Login from './screens/auth/login'
import Register from './screens/auth/register'
import Home from './screens/home'
import Settings from './screens/settings'
import Reports from './screens/reports'
import Goals from './screens/goals'
import BMLConnect from './screens/bmlconnect'
import StoreTransaction from './screens/transactions/StoreTransaction'
import UpdateTransaction from './screens/transactions/UpdateTransaction'
import StoreGoal from './screens/goals/StoreGoal'
import UpdateGoal from './screens/goals/UpdateGoal'


//define stacks
const HomeStack = createStackNavigator({
  Home: {screen: Home},
  BMLConnect: {screen: BMLConnect},
  UpdateTransaction: {screen: UpdateTransaction},
  StoreGoal: {screen: StoreGoal},
  UpdateGoal: {screen: UpdateGoal}
})

const ReportStack = createStackNavigator({
  Reports: {screen: Reports},
})

const AddNewStack = createStackNavigator({
  StoreTransaction: {screen: StoreTransaction},
})

const GoalsStack = createStackNavigator({
  Goals: {screen: Goals},
})

const SettingsStack = createStackNavigator({
  Settings: {screen: Settings},
})

const Tabs = createBottomTabNavigator({
  Feed: {
    screen: HomeStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./screens/icons/feed-icon.png')}
          style={[styles.icon, {tintColor: tintColor, width: 24, height: 24}]}
        />
      ),
    }
  },
  Reports: {
    screen: ReportStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./screens/icons/reports-icon.png')}
          style={[styles.icon, {tintColor: tintColor, width: 24, height: 24}]}
        />
      ),
    }
  },
  Add: {
    screen: AddNewStack,
    navigationOptions: {
      title: ' ',
      tabBarIcon: ({ tintColor }) => (
          <Image
            source={require('./screens/icons/add-icon.png')}
            style={{width: 64, height: 64, resizeMode: 'contain', bottom: -1, position: 'absolute'}}
          />
      ),
    }
  },
  Goals: {
    screen: GoalsStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./screens/icons/target-icon.png')}
          style={[styles.icon, {tintColor: tintColor, width: 24, height: 24}]}
        />
      ),
    }
  },
  Settings: {
    screen: SettingsStack,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./screens/icons/settings-icon.png')}
          style={[styles.icon, {tintColor: tintColor, width: 24, height: 24}]}
        />
      ),
    }
  },
}, {

    headerMode: 'none',
    cardStyle: {
      shadowOpacity: 0,
      backgroundColor: 'red'
    },
    tabBarPosition: 'bottom',
    initialRouteName: 'Feed',
    swipeEnabled: false,
    lazy: true,
    animationEnabled: false,
    tabBarOptions: {
    showLabel: true,
    showIcon: true,
    upperCaseLabel: false,
    activeTintColor: '#20C287',
    inactiveTintColor: '#444',
    inactiveBackgroundColor: '#FFFFFF',
    activeBackgroundColor: 'transparent',
    style: {
      backgroundColor: 'white',
      borderTopWidth: 0,
      borderColor: '#eee',
      height: 58
    },
    tabStyle: {
      backgroundColor: 'white',
      paddingTop: 8,
      height: 58

    },
    labelStyle: {
      paddingTop: 4,
      fontSize: 11
    }
}})

const AppRouteConfigs = ({
  Splash: {screen: Splash},
  Register: {screen: Register},
  Login: {screen: Login},
  Tabs: {screen: Tabs},
});

const Navigations = createStackNavigator({
  Splash: {screen: Splash},
  Register: {screen: Register},
  Login: {screen: Login},
  Tabs: {screen: Tabs},
}, {
    cardStyle: {
      shadowOpacity: 0,
      backgroundColor: '#20C287'
    },
    headerMode: 'none',
    tabBarPosition: 'bottom',
    initialRouteName: 'Splash',
    swipeEnabled: false,
    lazy: true,
    animationEnabled: false,
})

const fade = (props) => {
    const {position, scene} = props

    const index = scene.index

    const translateX = 0
    const translateY = 0

    const opacity = position.interpolate({
        inputRange: [index - 0.7, index, index + 0.7],
        outputRange: [0.3, 1, 0.3]
    })

    return {
        opacity,
        transform: [{translateX}, {translateY}]
    }
}

const styles = StyleSheet.create({
  icon: {
    width: 30,
    height: 30,
    resizeMode: 'contain'
  }
})

export { Navigations, AppRouteConfigs, fade }
