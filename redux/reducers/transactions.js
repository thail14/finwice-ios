import { FETCHING_TRANSACTIONS, FETCH_TRANSACTIONS_SUCCESS, FETCH_TRANSACTIONS_FAILURE } from '../helpers/constants';

const initialState = {
    payload: [],
    isFetching: false,
    error: false
}

export default function finwiceTransactionsReducer(state = initialState, action) {
  //  console.log(action.data)
    switch(action.type) {
        case FETCHING_TRANSACTIONS:
            return {
                ...state,
                isFetching: true
            }
        case FETCH_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                payload: action.data
            }
        case FETCH_TRANSACTIONS_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true
            }
        default:
            return state
    }
}
