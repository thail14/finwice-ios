import { FETCHING_GOALS, FETCH_GOALS_SUCCESS, FETCH_GOALS_FAILURE } from '../helpers/constants';

const initialState = {
    payload: [],
    isFetching: false,
    error: false
}

export default function finwiceGoalsReducer(state = initialState, action) {
  //  console.log(action.data)
    switch(action.type) {
        case FETCHING_GOALS:
            return {
                ...state,
                isFetching: true
            }
        case FETCH_GOALS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                payload: action.data
            }
        case FETCH_GOALS_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true
            }
        default:
            return state
    }
}
