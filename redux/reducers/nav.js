import AppNavigator from '../helpers/navigator'
import { createNavigationReducer } from 'react-navigation-redux-helpers';

export default navReducer = createNavigationReducer(AppNavigator);
