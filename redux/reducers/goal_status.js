import { FETCHING_GOAL_STATUS, FETCH_GOAL_STATUS_SUCCESS, FETCH_GOAL_STATUS_FAILURE } from '../helpers/constants';

const initialState = {
    payload: [],
    isFetching: false,
    error: false
}

export default function finwiceGoalStatusReducer(state = initialState, action) {
    switch(action.type) {
        case FETCHING_GOAL_STATUS:
            return {
                ...state,
                isFetching: true
            }
        case FETCH_GOAL_STATUS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                payload: action.data
            }
        case FETCH_GOAL_STATUS_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true
            }
        default:
            return state
    }
}
