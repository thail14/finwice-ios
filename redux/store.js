import {
  createStore,
  applyMiddleware,
  combineReducers,
} from 'redux'
import {
  StackNavigator,
  addNavigationHelpers,
  NavigationActions
} from 'react-navigation';
import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';
import thunk from 'redux-thunk';
import navReducer from './reducers/nav'
import finwiceTransactionsReducer from './reducers/transactions'
import finwiceGoalsReducer from './reducers/goals'
import finwiceGoalStatusReducer from './reducers/goal_status'

const appReducer = combineReducers({
  nav: navReducer,
  transactions: finwiceTransactionsReducer,
  goals: finwiceGoalsReducer,
  goal_status: finwiceGoalStatusReducer
});

const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);

export default store = createStore(
  appReducer,
  applyMiddleware(thunk),
)
