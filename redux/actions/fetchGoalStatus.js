import { FETCHING_GOAL_STATUS, FETCH_GOAL_STATUS_SUCCESS, FETCH_GOAL_STATUS_FAILURE } from '../helpers/constants';

export function fetchGoalStatus(){

    return (dispatch) => {
        dispatch(getGoalStatus())
        var url = apiHost + '/users/goals/status'
        return(fetch(url, { method: 'GET', headers: { 'Authorization': 'Bearer ' + JWT, 'Content-Type': 'application/json' } }))
        .then(res => res.json())
        .then(json => {
            return(dispatch(getGoalStatusSuccess(json)))
        })
        .catch(err => dispatch(getGoalStatusFailure(err)))
    }
}

function getGoalStatus() {

    return {
        type: FETCHING_GOAL_STATUS
    }
}

function getGoalStatusSuccess(data) {

    return {
        type: FETCH_GOAL_STATUS_SUCCESS,
        data
    }
}

function getGoalStatusFailure() {

    return {
        type: FETCH_GOAL_STATUS_FAILURE
    }
}
