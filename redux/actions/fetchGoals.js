import { FETCHING_GOALS, FETCH_GOALS_SUCCESS, FETCH_GOALS_FAILURE } from '../helpers/constants';

export function fetchGoals(){

    return (dispatch) => {
        dispatch(getGoals())
        var url = apiHost + '/users/goals'
        return(fetch(url, { method: 'GET', headers: { 'Authorization': 'Bearer ' + JWT, 'Content-Type': 'application/json' } }))
        .then(res => res.json())
        .then(json => {
            return(dispatch(getGoalsSuccess(json)))
        })
        .catch(err => dispatch(getGoalsFailure(err)))
    }
}

function getGoals() {

    return {
        type: FETCHING_GOALS
    }
}

function getGoalsSuccess(data) {

    return {
        type: FETCH_GOALS_SUCCESS,
        data
    }
}

function getGoalsFailure() {

    return {
        type: FETCH_GOALS_FAILURE
    }
}
