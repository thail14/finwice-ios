import { FETCHING_TRANSACTIONS, FETCH_TRANSACTIONS_SUCCESS, FETCH_TRANSACTIONS_FAILURE } from '../helpers/constants';


export function fetchTransactions(){

    return (dispatch) => {
        dispatch(getTransactions())
        var url = apiHost + '/users/transactions'
        return(fetch(url, { method: 'GET', headers: { 'Authorization': 'Bearer ' + JWT, 'Content-Type': 'application/json' } }))
        .then(res => res.json())
        .then(json => {
            return(dispatch(getTransactionsSuccess(json)))
        })
        .catch(err => dispatch(getTransactionsFailure(err)))
    }
}

function getTransactions() {

    return {
        type: FETCHING_TRANSACTIONS
    }
}

function getTransactionsSuccess(data) {
    return {
        type: FETCH_TRANSACTIONS_SUCCESS,
        data
    }
}

function getTransactionsFailure() {

    return {
        type: FETCH_TRANSACTIONS_FAILURE
    }
}
