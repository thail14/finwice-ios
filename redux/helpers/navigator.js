import { AppRouteConfigs, fade } from 'finwice/App';
import { createStackNavigator } from 'react-navigation';

export default AppNavigator = createStackNavigator(AppRouteConfigs,{
  transitionConfig: () => ({
        screenInterpolator: (props) => {
            return fade(props)
        }
  }),
  headerMode: 'none',
  mode: 'card',
  initialRouteName: 'Splash',
  cardStyle: {
    shadowOpacity: 0,
    backgroundColor: '#F5F8F5'
}})
